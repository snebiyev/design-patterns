package az.techlab.team;

public interface ProjectTeam {
    Developer getDeveloper();

    Tester getTester();

    ProjectManager getManager();
}
