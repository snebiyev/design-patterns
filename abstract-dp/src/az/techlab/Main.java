package az.techlab;

import az.techlab.team.ProjectTeam;
import az.techlab.webservice.WebServiceTeamFactory;
import az.techlab.website.WebSiteTeam;

public class Main {
    public static void main(String[] args) {
//        ProjectTeam team=new WebServiceTeamFactory();
        ProjectTeam team = new WebSiteTeam();
        System.out.println("Starting new Web Service Project");
        team.getManager().manage();
        team.getDeveloper().writeCode();
        team.getTester().testProject();

    }
}
