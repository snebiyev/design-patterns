package az.techlab.website;

import az.techlab.team.Developer;
import az.techlab.team.ProjectManager;
import az.techlab.team.ProjectTeam;
import az.techlab.team.Tester;

public class WebSiteTeam implements ProjectTeam {
    @Override
    public Developer getDeveloper() {
        return new PhpDeveloper();
    }

    @Override
    public Tester getTester() {
        return new ManualTester();
    }

    @Override
    public ProjectManager getManager() {
        return new SiteProjectManager();
    }
}
