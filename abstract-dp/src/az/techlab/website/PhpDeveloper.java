package az.techlab.website;

import az.techlab.team.Developer;

public class PhpDeveloper implements Developer {
    @Override
    public void writeCode() {
        System.out.println("PHP Developer started to write php code...");
    }
}
