package az.techlab.webservice;

import az.techlab.team.ProjectManager;

public class ServiceProjectManager implements ProjectManager {
    @Override
    public void manage() {
        System.out.println("Web service project manager started to manage team...");
    }
}
