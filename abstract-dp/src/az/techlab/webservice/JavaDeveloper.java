package az.techlab.webservice;

import az.techlab.team.Developer;

public class JavaDeveloper implements Developer {
    @Override
    public void writeCode() {
        System.out.println("The Java developer started writing code... ");
    }
}
