package az.techlab.webservice;

import az.techlab.team.Developer;
import az.techlab.team.ProjectManager;
import az.techlab.team.ProjectTeam;
import az.techlab.team.Tester;
import az.techlab.website.ManualTester;
import az.techlab.website.PhpDeveloper;

public class WebServiceTeamFactory implements ProjectTeam {
    @Override
    public Developer getDeveloper() {
        return new JavaDeveloper();
    }

    @Override
    public Tester getTester() {
        return new QATester();
    }

    @Override
    public ProjectManager getManager() {
        return new ServiceProjectManager();
    }
}
