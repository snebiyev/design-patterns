package az.techlab;

/*
 * https://www.javatpoint.com/factory-method-design-pattern
 */
public class Main {
    public static void main(String[] args)  {
        CarFactory factory=new CarFactory();

        Car kia=factory.get("kia");
        kia.startEngine();
        Car audi=factory.get("audi");
        audi.startEngine();
        Car qaz=factory.get("qaz");
        try {
            qaz.startEngine();
        }
        catch (NullPointerException e){
            System.out.println("Object is null");
        }

        Car nullModel=factory.get(null);
        try {
            nullModel.startEngine();
        }
        catch (NullPointerException e){
            System.out.println("Object is null");
        }

    }
}
