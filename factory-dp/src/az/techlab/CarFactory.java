package az.techlab;

import java.util.Locale;

public class CarFactory {

    public Car get(String car) {

        String carModel;
        if (car == null) {
            return null;
        } else {
            carModel = car.toLowerCase(Locale.ROOT);
        }
        if (carModel.equals("kia")) {
            return new Kia();
        } else if (carModel.equals("audi")) {
            return new Audi();
        }

        return null;
    }
}
