package az.techlab;

public abstract class Car {
    protected String engine;

    abstract void startEngine();

    public Integer getSpeed() {
        return 100;
    }
}
