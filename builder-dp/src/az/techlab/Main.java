package az.techlab;

public class Main {
    public static void main(String[] args) {
        Car rav4 = new Car.CarBuilder("Toyota", "RAV4").color("pearl").build();
        System.out.println(rav4);
    }
}
