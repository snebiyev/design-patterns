package az.techlab;

import java.util.concurrent.atomic.AtomicInteger;

public class Troll {
    private static final AtomicInteger counter = new AtomicInteger(0);

    private final int id;

    public Troll() {
        id = counter.incrementAndGet();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return String.format("Game Troll id=%d", id);
    }
}
