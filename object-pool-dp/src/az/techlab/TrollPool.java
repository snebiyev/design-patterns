package az.techlab;

public class TrollPool extends ObjectPool<Troll> {

    @Override
    protected Troll create() {
        return new Troll();
    }
}
