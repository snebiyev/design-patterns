package az.techlab;

public class Main {
    public static void main(String[] args) {
        TrollPool pool = new TrollPool();
        Troll troll1 = pool.checkOut();
        Troll troll2 = pool.checkOut();
        Troll troll3 = pool.checkOut();
        pool.checkIn(troll1);
        pool.checkIn(troll2);
        Troll troll4 = pool.checkOut();
        Troll troll5 = pool.checkOut();
    }
}
